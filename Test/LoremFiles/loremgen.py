import lorem
import random


i = 1
j = 0

while i <= 100000:
    f = open("D:\Examples\lorem100000\lorem%i.txt" % i, "x")
    para = lorem.text()
    rand = random.randint(15, 20)
    while j < rand:
        para += lorem.text()
        j += 1
    j = 0
    f.write(para)
    i += 1
