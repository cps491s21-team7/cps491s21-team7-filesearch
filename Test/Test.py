import ngram
import threading
import time
import os
import sys
import getopt
import hashlib
import mimetypes
from tabulate import tabulate
from progress.bar import ChargingBar
thread_lock = threading.Lock()
shared_Files = []
searchElements = []
threads = []
search_Threads = []
search_map = {}
key = " "  # the user search input
foundDestination = set()
vas= set()
count = 0
already_visited = set()
destinations = {}
# thread to parse incoming files
indexcounter = 0
file_name = "indexed.txt"
index_number = 0
path = ""


class parser (threading.Thread):

    # initialize thread
    def __init__(self, threadID, name, input_length, mode):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.files = []
        self.lines = []
        self.parse_length = input_length
        self.mode = mode  # determines whether we index or search on the fly

    # running protocol of thread
    def run(self):
        # take file from shared queue of files
        # print("running", self.name)
        # thread_lock.acquire()

        while True:

            if len(shared_Files) == 0:
                break
            else:
                if self.mode == 0:
                    self.parseLines(shared_Files.pop())

                else:
                    
                    self.indexFile(shared_Files.pop())
                   
                    # index the file

    # used for indexing approach - index file into n elements
    # combines n files into 1 file with indexed elements

    def indexFile(self, theFile):
        # see if indexed file exists - for safety
        temp = []
        # current_path = sys.argv[1]
        current_path = path
        complete_path = os.path.join(current_path, file_name)
        global indexcounter
        if os.path.exists(complete_path):
            try:
                File = open(theFile, "rb")
            except FileNotFoundError:
                pass
            else:
                # acquire lock (safety) and append individual elements

                # first determine what type of file is at hand
                # if binary, we need to read byte by byte otherwise line by line

                mime = mimetypes.guess_type(theFile)
                guess = str(mimetypes.guess_extension(mime[0]))
                # assume if not binary, txt file
                i = 0
                if guess == ".txt":

                    for words in File:
                        line = words.split()
                        for i in line:
                            ihex = i.hex()
                            temp.append(ihex)
                else:
                    with open(theFile, "rb") as f:
                        byte = f.read(1)
                        while byte != b"":
                            byte = f.read(1)
                            byte_hex = byte.hex()
                            temp.append(byte_hex)
                File.close()
            try:
                write_file = open(complete_path, "a")
            except FileNotFoundError:
                pass
            else:
                thread_lock.acquire()
                global index_number
                # theFile = theFile.replace(" ", "")

                # for all words in file
                for i in range(len(temp)):
                    indexcounter = indexcounter + 1
                    #write_file.write(words + "-" + theFile + " ")
                    VA = id(temp[i])
                    write_file.write(temp[i] + "$" + shorten(theFile) + "!" + hex(VA))

                    # save immediate neighbors of element in file - used for multi word

                    # append neighbors to current element, which are next x (arbitrary) many elements

                    end = i + 3
                    start = i + 1 

                    temp2 = []
                    while start < end:
                        if end < len(temp): 
                            write_file.write("*" + temp[start])
                            temp2.append(temp[start])

                        start = start + 1
                    write_file.write(" ")

                    # complete implementation of this explained in calibrate_search() function
                    element = matchElement(temp[i])
                    the_element = search_map.get(temp[i])

                    if temp[i] in search_map:

                        new_location = ""  
                        num_locations = len(the_element.locations)
                        the_element.set_locations(theFile)
                        new_num = len(the_element.locations)
                        the_element.set_va(hex(VA))

                        if num_locations == new_num:
                            new_location = theFile + str(the_element.number + 1)
                            the_element.update_number(1)
                            the_element.set_locations(theFile)
                        for i in range(len(temp2)):
                            if len(new_location) > 0:
                                the_child = child_cl(temp2[i], new_location)
                            if len(new_location) == 0:
                                the_child = child_cl(temp2[i], theFile)
                            the_child.set_parent_file(theFile)
                            the_element.set_children(the_child)

                    if temp[i] not in search_map:
                        element.set_locations(theFile)
                        element.set_va(hex(VA))
                        search_map[temp[i]] = element

                        for i in range(len(temp2)):
                            the_child = child_cl(temp2[i], theFile)
                            the_child.set_parent_file(theFile)
                            element.set_children(the_child)
                    # =============================================

                
                write_file.write("\n")
                write_file.close()
                IndexFile.next()
                index_number = index_number + 1
               


                thread_lock.release()
    # parse file into ngrams, return list

    def parseLines(self, theFile):
        overflow_lines = []
        search_string = " "
        # print(theFile, "in thread ", self.name)
        # current_path = sys.argv[1]
        current_path = path
        complete_path = os.path.join(current_path, theFile)
        global searchElements
        global already_visited
        global destinations
        global indexcounter
        if os.path.exists(complete_path):
            mime = mimetypes.guess_type(theFile)
            guess = str(mimetypes.guess_extension(mime[0]))
            #  File = open(theFile, "r")

            # mime = mimetypes.guess_extension
            # print(theFile)
            # thread_lock.acquire()
        if guess == ".txt" and not is_hex(key):
            
            File = open(theFile, "r")
            for words in File:
                line = words.split()

                # if one element search, only append elements of line with matching size to possibly speed search

                if key.find(" ") == -1:
                    for i in range(len(line)):
                        size = len(line[i])

                        # if beginning character of key is not in begining spot of target string, forget it
                        # i.e first character of pizza 'p' compared to string izzap p =/= i (pizza will never be izzap)

                        first_character = key[0]
                        same = line[i][0] is first_character

                        # then check to see if element is already in search elements, if it is, do not add (avoid duplicates)

                        # attempt adding newest element to set - if size remains same, elements has already been added to search_elements
                        current_size = len(already_visited)
                        already_visited.add(line[i])

                        if current_size == len(already_visited):
                            # already added to search elements
                            visited = 1

                        if current_size != len(already_visited):
                            # not added to search elements
                            visited = 0

                        if len(key) == size:
                            element = line[i]
                            the_term = destinations.get(element)

                            if same:
                                
                                VA = id(line[i])
                                if visited == 0:
                                    # create new element and add it to destination set

                                    the_element = matchElement(element)
                                    the_element.set_locations(theFile)
                                    the_element.set_va(hex(VA))
                                    searchElements.append([element])
                                    destinations[element] = the_element
                                    already_visited.add(element)

                                if visited == 1:
                                    
                                    if the_term is not None:
                                        
                                        thread_lock.acquire()
                                        the_term.set_locations(theFile)
                                        
                                        the_term.set_va(hex(VA))
                                        thread_lock.release()

                # if size of key is greater than one, we want a multi word search
                if key.find(" ") != -1:





                    # length of the current line from file
                    line_size = len(line)
                    # number of elements (individual words) in search string
                    key_size = len(key.split())
                    # the number of leftover words from built search string
                    overflow = key_size - line_size

                    # first check to see if there's any overflow leftover from previous line
                    # for i in overflow_lines:
                    #     search_string += i + " "

                    # if not or after that then begin concatenating elements of current line
                    for x in range(line_size):
                        overflow = line_size - x

                        # if string has already reached limit (i.e due to past overflow) break and add all remaining elements in line to overflow

                        if len(search_string.split()) > key_size:

                            for overflow in range(line_size):
                                overflow_lines.append(line[overflow])
                            break

                        # if number of words less than number of words in search input string, concatenate

                        if x < key_size:
                            search_string += line[x] + " "
                        else:
                            overflow_lines.append(line[x])

                    searchElements.append(search_string)
                    search_string = " "
        if guess != ".txt" and is_hex(key):
            
            with open(theFile, "rb") as f:
                byte = f.read(1)
                while byte != b'':
                    byte = f.read(1)
                    byte_hex = str(byte.hex())

                    first_character = key[0]
                    try:
                        same = byte_hex[0] is first_character
                    except:
                        pass
                    current_size = len(already_visited)
                    already_visited.add(byte_hex)

                    if current_size == len(already_visited):
                        # already added to search elements
                        visited = 1
                    if current_size != len(already_visited):
                        # not added to search elements
                        visited = 0
                    
                    element = byte_hex
                    the_term = destinations.get(element)
                    if same:
                        
                        if visited == 0:
                            # create new element and add it to destination set
                            the_element = matchElement(element)
                            the_element.set_locations(theFile)
                            VA = id(byte)
                            the_element.set_va(hex(VA))
                            searchElements.append([element])
                            destinations[element] = the_element
                            already_visited.add(element)
                        if visited == 1:
                            
                            if the_term is not None:
                                
                                thread_lock.acquire()
                                the_term.set_locations(theFile)
                                VA = id(VA)
                                the_term.set_va(hex(VA))
                                thread_lock.release()
# thread to search recently parsed lines
class matchThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        time.sleep(0.5)
        
        while True:
            if len(searchElements) == 0:
                break
            else:
                line = searchElements.pop()
                search = ngram.NGram(line)
                found = search.search(key, 0.3)
                if found:
                    
                    element = destinations.get(line[0])
                    locations = element.locations
                    thread_lock.acquire()
                    for i in locations:
                        foundDestination.add(i)
                    for x in element.va:
                        vas.add(x)
                    thread_lock.release()
# if we have indexed file, load elements into map


class calibrate_map(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        time.sleep(1)

        # load elements from indexed file into map

# a class of search element that keeps track of all locations


class matchElement:
    def __init__(self, term):
        self.search_term = term
        self.number = 0
        self.locations = set()
        self.va = set()
        self.children = [] # a list of neighboring combinations of word (for multi word searches)
        # a list of all locations (files) where an element appears

    def set_locations(self, location):
        self.locations.add(location)

    def set_children(self, child):
        self.children.append(child)

    def set_va(self, va):
        self.va.add(va)

    def update_number(self, num):
        self.number = self.number + num
class loc:
    def __init__(self, dup, name):
        self.name = name
        self.dup = dup
# helper class for multi word searching
class child_cl:
    def __init__(self, term, location):
        self.term = term
        self.location = location
        self.parent_file = ""
    def set_parent_file(self, the_file):
        self.parent_file = the_file

# used to get hash of file
def md5(fname):
    hash_md5 = hashlib.md5()
    if fname.find("$") == -1:
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
    return hash_md5.hexdigest()

# function used to cut down paths to store in indexed file
def shorten(string):
    indices = [i for i, x in enumerate(string) if x == "\\"]

    name = string[indices[len(indices)-1]: len(string)]


    return name

# for input exceeding 8 words, breaks the input string into 8 word segments
def break_input(string):

    the_string = ""
    the_strings = []
    new_key = string.split(" ")
    for i in range(len(new_key)):

        if i == len(new_key) - 1:
            the_string = the_string + " " + new_key[i]
            the_strings.append(the_string)

        elif i % 2 == 0 and i != 0:
            the_strings.append(the_string)
            the_string = new_key[i] + " "
        else:
            if (i + 1) % 2 == 0 and i != 0:
           
                the_string = the_string + new_key[i]
            else:
           
                the_string = the_string + new_key[i] + " "

    return the_strings

# function for formatted output
def Output(search_result, start_time, indexed, element):
    locations = []
    md5array = []
    VA = []
    if indexed:
        print("%s results returned in %s ms" % (
            len(search_result), round(time.time() - start_time) * 1000))
        table = []
        # commented out for now
        # if len(sys.argv) > 3 and sys.argv[3] == '-o':
        #     print("Results are in: " + sys.argv[4])
        #     sys.stdout = open(sys.argv[4], 'w')

        for item in search_result:
            if item.find("$") == -1:
                locations.append(item)
                md5array.append(md5(item))

       
        for item in element.va:
            
            VA.append(item)
        i = 0
        while i < len(locations):
            table.append([locations[i], md5array[i], VA[i]])
            i += 1
        print(tabulate(table, headers=[
              "File", "MD5", "VA"], tablefmt="orgtbl"))
    else:
        print("%s results returned in %s ms" %
              (len(search_result), round(time.time() - start_time) * 1000))
        table = []
        # if len(sys.argv) > 3 and sys.argv[3] == '-o':
        #     print("Results are in: " + sys.argv[4])
        #     sys.stdout = open(sys.argv[4], 'w')
        for item in search_result:
            table.append([item, md5(item), vas.pop()])
        print(tabulate(table, headers=[
              "File", "MD5", "VA"], tablefmt="orgtbl"))

# converts binary hex input to hex string    

def Hexdec(key):
    # if the first letter of string is \x then its in hexdec
    newkey = ""
    if (key.find("\\", 0, 1) == 0 and key.find("x", 1, 2) == 1):
        for i in range(len(key)):
            if (key[i] != "\\" and key[i] != "x"):
                newkey = newkey + key[i]    
        
        return newkey
    if is_hex(key):
        return key

    return bytes(key.encode('ascii')).hex()

# checks to see whether a string is hex or not
def is_hex(check):
    try:
        int(check, 16)
        return True
    except ValueError:
        return False  

# calibrate search map 
def calibrate_search():
    print("Calibrating..")
    search_map = {}
    # calibrate_bar = ChargingBar('Calibrating', max=index_number)
    file = open(os.path.join(current_path, file_name), "r")
        # Calibrating = ChargingBar('Calibrating', max=indexcounter)
    
    for words in file:
        line = words.split()
        # calibrate_bar.next()
        x = 0
        for a_word in line:
            
            # find delimator between various elements
            
            delim = a_word.find("$")
            delim_star = a_word.find("*")
            delim_hat = a_word.find("!")

            search_term = a_word[0:delim]
            location = a_word[delim+1:delim_hat]
            
            # location = sys.argv[1] + location
            location = path + location
            va = a_word[delim_hat+1:delim_star]
            element = matchElement(search_term)
            the_element = search_map.get(search_term)
            
            # check to see if search term is in map first
            if search_term in search_map:
                new_location = ""
                # only add location if not there (add to set)
                num_locations = len(the_element.locations)
                the_element.set_locations(location)
                new_num = len(the_element.locations)

                # we are in same file but there's more children for same element 
                # i.e we found 54 twice in the same file
                if num_locations == new_num:
                    new_location = location + str(the_element.number + 1)
                    the_element.update_number(1)
                    the_element.set_locations(new_location +"$")

                the_element.set_va(va)
                indices = [i for i, x in enumerate(a_word) if x == "*"]
                
                # for all children (* indicated words) add to the children of current element
                for i in range(len(indices)):

                    if i + 1 >= len(indices):
                        child = a_word[indices[i]: len(a_word)]
                    else:
                        child = a_word[indices[i]: indices[i + 1]]
                    
                    child_star = child.replace("*", "")
                    
                    # in other words, if in same file but another occurence of same element
                    # we append child with a "new" location (so that the search works)
                    if len(new_location) > 0:

                        the_child = child_cl(child_star, new_location)

                    if len(new_location) == 0:
                        the_child = child_cl(child_star, location)

                    the_child.set_parent_file(location)
                    
                    the_element.set_children(the_child)
                

            if search_term not in search_map:
                # if not then append new term to map

                element.set_locations(location)
                element.set_va(va)
                search_map[search_term] = element
                indices = [i for i, x in enumerate(a_word) if x == "*"]
                
                # for 5 neighbors of each word add them the children of current element

                for i in range(len(indices)):

                    if i + 1 >= len(indices):
                        child = a_word[indices[i]: len(a_word)]
                    else:
                        child = a_word[indices[i]: indices[i + 1]]

                    child_star = child.replace("*", "")
                    the_child = child_cl(child_star, location)
                    the_child.set_parent_file(location)
                    element.set_children(the_child)

    # calibrate_bar.finish()
    return search_map

# helper function to do searches on map
def make_search(the_key, key, flag):
    # if it is in \x format then split hex string up
    if (the_key.find("\\", 0, 1) == 0 and the_key.find("x", 1, 2) == 1):
        the_key = ""
        the_key = ' '.join(key[i:i+2] for i in range(0, len(key), 2))
        
    if the_key.find(" ") != -1:

        start_time = time.time()
        match_term = the_key.split()
        
        # first check to see if first element of search string is already hex

        if is_hex(match_term[0]):
            search_result = search_map.get(match_term[0])

        else:

            search_result = search_map.get(bytes(match_term[0].encode('ascii')).hex())
            match_term[0] = str(bytes(match_term[0].encode('ascii')).hex())  

        if search_result is not None:
            # get children of first word of entered phrase   
            children = search_result.children
            test_key = the_key.replace(" ", "") # for easier comparrison
            phrase = match_term[0]
            if not is_hex(test_key):
             
                sth = bytes(test_key.encode('ascii')).hex()
                test_key = sth
        
            # if there are more than 1 locations, check each location for key
            if len(search_result.locations) > 1:
                the_locations = [] # keep track of all locations
    
                phrase = match_term[0]   
                
                # check all children and append to phrase one at a time
                
                for i in range(len(children)):
                    
                    # retrieve the location of neighboring child
                    if i + 1 < len(children):
                        next_location = children[i+1].location
                    if i + 1 > len(children):
                        next_location = children[i].location
                    
                    # if the location is the same, it is same child, continue to append phrase
                    if children[i].location == next_location:
                        phrase = phrase + children[i].term
                        
                    
                    # if phrase is key or phrase from new location (different file)
                    
                    if phrase == test_key or children[i].location != next_location:
                        if phrase == test_key: 
                            if children[i].parent_file not in the_locations:
                                the_locations.append(children[i].parent_file)
                        phrase = match_term[0]
                if flag != 1:

                    Output(the_locations,start_time, True, search_result) 
                else:
                    return the_locations        
            else:
                # append children to length of key and test phrase
                
                for i in range(len(match_term) - 1):
                    if i != len(match_term) - 2:
                        phrase = phrase + children[i].term
                    else:
                        phrase = phrase + children[i].term 
                
                
                # check to see if test phrase is key

                if phrase == test_key:
                    
                    if flag != 1:

                        Output(search_result.locations, start_time, True, search_result) 
                    else:
                    
                        return search_result.locations
        else: 
            print("Item not found")

    # if key has no spaces (single word search)
    if the_key.find(" ") == -1:
        start_time = time.time()

        search_result = search_map.get(key)
        
        if search_result is not None:
            if flag != 1:

                Output(search_result.locations, start_time, True, search_result) 
            else:
                return search_result.locations
        else:
            print("Item not found")

# if input is over 8 words, split input and then attempt to search
def split_search(test_key, flag):
#    new_input = test_key
   if flag == 0:
        new_input = break_input(the_key)
        the_locations = []
        sets = []
        match_terms = []
        # if there are three segments and the file in question is file1, then file1 must appear 3 times in locations
        for x in range(len(new_input)):
            
             
         #    the_locations.append(make_search(new_input[x], key, 1))
             the_locations.append(make_search(new_input[x], key, 1))
             result = make_search(new_input[x], key, 1)


             if result == None:
                 print("Item not found")
                 return
             if is_hex(new_input[x].split()[0]):
                 term = search_map.get(new_input[x].split()[0])
             if not is_hex(new_input[x].split()[0]):
                 term = search_map.get(bytes(new_input[x].split()[0].encode('ascii')).hex())
             match_terms.append(term)
   if flag != 0:
        new_input = test_key
        the_locations = []
        sets = []
        match_terms = []
    
        for x in range(len(new_input)):
            the_locations.append(make_search(new_input[x], new_input[x], 1))
            result = make_search(new_input[x], new_input[x], 1)


            if result == None:
                print("Item not found")
                return
            if is_hex(new_input[x].split()[0]):
                term = search_map.get(new_input[x].split()[0])
            if not is_hex(new_input[x].split()[0]):
                term = search_map.get(bytes(new_input[x].split()[0].encode('ascii')).hex())
            match_terms.append(term)

    
   # find all common files between each segment (use set intersection)
   if len(the_locations) > 0:
    for i in the_locations:
        sets.append(set(i))
   match_locations = list(set.intersection(*sets))
   # Don't think the va is correct
   if len(match_locations) > 0:
    Output(match_locations, start_time, True, term)                         

if __name__ == "__main__":
    
    # current_path = sys.argv[1]
    path = input("Enter the current directory of files ")
    current_path = path
    complete_path = []

    # first check to see if path exists, if not continue to ask until given path exists
    if os.path.exists(current_path):

        for entry in os.listdir(current_path):
            if os.path.isfile(os.path.join(current_path, entry)):
                complete_path.append(entry)
        # check to see if we have an indexed file
        indexed = os.path.exists(os.path.join(current_path, file_name))

    # keep asking until valid path    
    if not os.path.exists(current_path):
        print("Could not find path. Please try again ")
        while True:
            path = input("Enter the current directory of files ")
            current_path = path
            if os.path.exists(current_path):
                for entry in os.listdir(current_path):
                    if os.path.isfile(os.path.join(current_path, entry)):
                        complete_path.append(entry)
                indexed = os.path.exists(os.path.join(current_path, file_name))
                break
            else:
                continue



    if indexed:
        # then simply do a search on this map with key - O(1)
        search_map = calibrate_search()

        # continually search while user wants
        while True:
            key = input("Enter Word/Phrase to search for, press x to exit:  ")
            the_key = key # used to determine if multi word search
            original_key = key
            key = Hexdec(key) # convert user input to hex
            
            start_time = time.time()
            # check to see if key > 8 if so, split it up
            test_key = key.split()
            the_key.replace(" ", "")
            
            if (the_key.find("\\", 0, 1) == 0 and the_key.find("x", 1, 2) != -1):
                test_key = ""
                test_key= [key[i:i+2] for i in range(0, len(key), 2)]
                test_key2 = ""
                for i in test_key:
                    test_key2 = test_key2 + i
                

            
            if len(the_key) > 2 or the_key.find("\\", 0, 1) == 0 and the_key.find("x", 1, 2) != -1:
                if (original_key.find("\\", 0, 1) == 0 and original_key.find("x", 1, 2) == 1):
                    
                    split_search(test_key, 1)
                else:
                    split_search(test_key, 0)
            if key == "78":
                break
            if len(key) == 0:
                print("Please enter a term to search for")
                continue
            if len(test_key) <= 2:
                print("here")
                
                make_search(the_key, key, 0)
    else:
        # define needed variables
        i = 1
        input_size = len(key)

        response = input(
            "No indexed file was found.. press 'x' to continue  search or 'i' to index files: ")
        start_time = time.time()

        if response == "i":
            # if user requests index, first create an index file, then index the files
            file = open(os.path.join(current_path, file_name), "w")
            file.write("0")
            file.close()
            for File in complete_path:
                shared_Files.append("%s\%s" % (current_path, File))
            IndexFile = ChargingBar('Indexing', max=len(shared_Files))
            t1 = parser(1, "Thread1", 0, 1)
            t2 = parser(2, "Thread2", 0, 1)
            t3 = parser(3, "Thread3", 0, 1)
            t4 = parser(4, "Thread4", 0, 1)
            t5 = parser(5, "Thread1", 0, 1)
            t6 = parser(6, "Thread2", 0, 1)
            t7 = parser(7, "Thread3", 0, 1)
            t8 = parser(8, "Thread4", 0, 1)
            threads.append(t1)
            threads.append(t2)
            threads.append(t3)
            threads.append(t4)
            threads.append(t5)
            threads.append(t6)
            threads.append(t7)
            threads.append(t8)

            for thread in threads:
                thread.start()
            for the_thread in threads:
                the_thread.join()

            IndexFile.finish()
            # search_map = calibrate_search()

            # continually search as long as user wants
            while True:
                key = input("Enter Word/Phrase to search for, press x to exit:  ")
                the_key = key # used to determine if multi word search
                original_key = key
                key = Hexdec(key) # convert user input to hex

                start_time = time.time()
                # check to see if key > 8 if so, split it up
                test_key = key.split()
                the_key.replace(" ", "")

                if (the_key.find("\\", 0, 1) == 0 and the_key.find("x", 1, 2) != -1):
                    test_key = ""
                    test_key= [key[i:i+2] for i in range(0, len(key), 2)]
                    test_key2 = ""
                    for i in test_key:
                        test_key2 = test_key2 + i



                if len(the_key) > 2 or the_key.find("\\", 0, 1) == 0 and the_key.find("x", 1, 2) != -1:
                    if (original_key.find("\\", 0, 1) == 0 and original_key.find("x", 1, 2) == 1):

                        split_search(test_key, 1)
                    else:
                        split_search(test_key, 0)
                if key == "78":
                    break
                if len(key) == 0:
                    print("Please enter a term to search for")
                    continue
                if len(test_key) <= 2:
                
                    make_search(the_key, key, 0)

        else:
            key = input("Enter Word/Phrase to search for:  ")
            # key = sys.argv[2]
            the_key = key # used to determine if multi word search
            # key = Hexdec(key) # convert user input to hex
            test_key = key.split()
            if not is_hex(key) and len(test_key) <=1:
                for File in complete_path:
                    shared_Files.append("%s\%s" % (current_path, File))
                start_time = time.time()
                t1 = parser(1, "Thread1", input_size, 0)
                t2 = parser(2, "Thread2", input_size, 0)
                t3 = parser(3, "Thread3", input_size, 0)
                t4 = parser(4, "Thread4",  input_size, 0)
                search1 = matchThread("search1")
                search2 = matchThread("search2")
                search3 = matchThread("search3")
                search4 = matchThread("search4")

                t1.start()
                t2.start()
                t3.start()
                t4.start()
                search_Threads.append(search1)
                search_Threads.append(search2)
                search_Threads.append(search3)
                search_Threads.append(search4)
                threads.append(t1)
                threads.append(t2)
                threads.append(t3)
                threads.append(t4)
                for thread in threads:
                    thread.join()
                search1.start()
                search2.start()
                search3.start()
                search4.start()
                for thread in search_Threads:
                    thread.join()
                Output(foundDestination, start_time, False, vas)
    # if len(sys.argv) > 3 and sys.argv[3] == '-o':
    #     print("Results are in: " + sys.argv[4])
    #     sys.stdout = open(sys.argv[4], 'w')

    # end non-indexing implementation