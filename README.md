University of Dayton

Department of Computer Science

CPS 490 - Spring, 2021

## Capstone II Project

# Project Title

Efficient File Content Searching at Scale

# Team members

1. Andrew Streng, strenga1@udayton.edu
2. Roberto Valadez, valadezjrr1@udayton.edu
3. Nolan Hollingsworth, hollingsworthn2@udayton.edu

# Company Mentors

Jeff Archer-Jeffrey.Archer@ge.com

GE Aviation

Company address

## Project homepage

<https://cps491s21-team7.bitbucket.io/>

## Project SourceCode

<https://bitbucket.org/cps491s21-team7/cps491s21-team7-filesearch/>

# Overview

![Overview Architecture](imgs/project-overview.PNG "A Sample of Overview Architecture")

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

For this team's project, we will be partnering with GE Aviation in order to create/use a tool that takes binary code sequence-searches samples for malware, returns ASCII/Unicode matches, as well, returns any lists of matched file paths and/or matched sequences.
The context that this project is ultimately being developed for is so that GE aviation has an efficient tool to search Malware samples for a specific piece.

# Use Cases

![UseCaseDiagram.PNG](imgs/UseCaseDiagram.PNG "UseCaseDiagram")

1. Use Case: Input Data

   - Actor: User
   - User Story: As a User, I want to chose some data to be searched.
   - Use Case Description: User inputs some data to be searched as fast as possible for related data.

2. Use Case: Search String Data

   - Actor: User
   - User Story: As a User, I want to see if a certain string suspected to be malware occurs in a data sample.
   - Use Case Description: Data is being searched and realated data is being shown.

3. Use Case: Search Binary Profile

   - Actor: User
   - User Story: As a User, I want to see if a set of binary instructions intended to produce malicious activity exists in data sample.
   - Use Case Description: Binary element is converted to ngrams to be processed by ngram libary.

4. Use Case: Find matching malware suspects

   - Actor: User
   - User Story: As a User, I want to know the exact location of a malware sample in file in order to take necessary actions and the matching samples.
   - Use Case Description: The methods in library will return matching sample in file.

5. Use Case: Search Hexadecimal Profile

   - Actor: User
   - User Story: As a User, I want to see if a set of Hexadecimal instructions intended to produce malicious activity exists in data sample.
   - Use Case Description: Hexadecimal element is converted to ngrams to be processed by ngram libary.

6. Use Case: Find matching malware suspects

   - Actor: User
   - User Story: As a User, I want to know the exact location of a malware sample in file in order to take necessary actions and the matching samples.
   - Use Case Description: The methods in library will return matching sample in file.

# High-level Requirements

Some our possible high-level requirements for uses cases include:

1. Be able to add "personalized" search criteria for the malware. Not just search criteria provided through development.

2. Be able to take certain actions on piece of malware. Maybe it needs to be kept track of in a database.

3. Create interactive menu for malware testing, such as a file explorer popup to add target files.

# Technology

Our main technologies that will be focused on are:

1. Linux environment
2. Python
3. Database of Malware
4. N-gram (or possibly self developed search algorithm)

For our resources, there will be:

1. VirusTotal’s search algorithm
2. MITRE’s Snugglefish tool
3. The YARA project

# Implementation

# Basic search of user defined field through database

## Sprint 1 Implementation:

User inputs a search string and is processed through a simulated database, which is a collection of files. The current implementation takes each line from input and creates ngrams
from data in line. This input is then analyzed on a level of similarity between all elements within the line.

### Pass all elements of file to ngram function to determine if it matches

![Sprint 1 Implementation](imgs/code-sprint1.PNG "Sprint 1 Code")

## #Sprint 2 Implementation:

User inputs a FilePath to where the database is located at. User inputs a search string and is processed through a simulated database. We have a variety of database from 50, 100,1000 files. The current implementation takes each line splits it into muliple strings and searched through. It also uses thread to decrease the time to find the object.

### The threads will empty files from shared queue and parse them until there are no files left

![Sprint 2 Implementation](imgs/Implementation-Sprint2-1.PNG "Implementation-Sprint2-1")
![Sprint 2 Implementation](imgs/Implementation-Sprint2-2.PNG "Implementation-Sprint2-2")

## Sprint 3 Implementation:

User will now be able to search Ascii/characters. They will also now be able to index their database to where the time to search an object will be cut drastically.We also optimized the threading to be able to search faster and more efficient. Lastly we added the ability to output the search to a file and also added a progress bar to the search.\

### All elements will be also included in an indexed file and loaded into a map for 0(1) searches

![Sprint 3 Implementation](imgs/Implementation-Sprint3-1.PNG "Implementation-Sprint3-1")
![Sprint 3 Implementation](imgs/Implementation-Sprint3-2.PNG "Implementation-Sprint3-2")

## Sprint 4 Implementation:

User will now be able to search through EXE files. This is allowed since we turned all inputs into Ascii and storing them in the map. We made the progress bar for the indexing. This takes a little bit longer since we combined the indexing and mapping together. Overall this speeds up the process of searching for a object in the indexed file.

### Map Calibration and Indexing now happen at the same time

![Sprint 4 Implementation](imgs/Implementation-Sprint4-1.png "Implementation-Sprint4-1")

# Scrum Process

## Sprint 1

Duration: 01/20/2021-2/15/2021

Completed Tasks:

1.  Python Ngram setup
2.  Basic input processed in small sample size

Contributions:

1.  Roberto Valadez, 6 hours, Ngram library setup, debugging

2.  Nolan Hollingsworth, 4 hours, Ngram libary setup, debugging

3.  Andrew Streng, 4 hours, debugging, readme/trello updates

Sprint 1 Retrospection:
For Sprint 1, we believe we have met our goals. The primary goal of this Sprint was to have the library setup. In addition to completing this, we have
also got a basic level of input working. The performance of Sprint 1 is summarized below.

Good: Achieved Sprint Goal

Could Have Done Better: n/a

Area of Improvement: Greater time/work investment

## Sprint 2

Duration: 2/16/2021-3/15/2021

Completed Tasks:

1. Basic threading for start of efficiency
2. Basic multi string input

Contributions:

1.  Roberto Valadez, 10 hours, dynamic filepath setup , debugging

2.  Nolan Hollingsworth, 10 hours, multiple line input, debugging

3.  Andrew Streng, 10 hours, threading

Sprint 2 Retrospection:
Duration: 2/16/2021-3/15/2021

Sprint 2 Retrospection: For Sprint 2, we believe we have met our goals. The primary goal of this Sprint was to show signs of efficieny improvement. The performance of Sprint 2 is summarized below.

Good: Achieved Sprint Goals

Could Have Done Better: n/a

Area of Improvement: We could have done a little more on the efficieny end.

## Sprint 3

Duration: 3/15/2021 - 4/6/2021

Completed Tasks:

1. Input into hex/ascii characters
2. Indexing searches
3. Threading searches optimization
4. Progress bar
5. argument flags

Contributions:

1.  Roberto Valadez, 10 hours README, stdout flags ,progress bar, bug fixes

2.  Nolan Hollingsworth, 12 hours Input into hex/ascii characters

3.  Andrew Streng, 12 hours Indexing searches / Threading searches optimization

Sprint 3 Retrospection: This Sprint was a optimization/Input overhaul. We also added a indexing option to all rapid results. This will have to be used every once in a while to update the index. We also added the ability to have hex/ascii characters to be searched.

Good: Achieved Sprint Goals

Could Have Done Better: n/a

Area of Improvement: Better output management

## Sprint 4

Duration: 4/6/2021 - 4/30/2021

Completed Tasks:

1. Website finished
2. Input into hex/ascii characters
3. binary file handler
4. Created executable
5. Combined indexing & calibrating
6. Multiword input

Contributions:

1.  Roberto Valadez, 14 hours README, Website, Input hex/ascii

2.  Nolan Hollingsworth, 14 hours Multiword input,Input hex/ascii

3.  Andrew Streng, 16 hours binary file handler,executable, bugfixing

Sprint 3 Retrospection: In this sprint, we completed a couple of big tasks. For example, we are able to search our samples for multiple word phrases. This is especially useful for phrases that represent a set of hex instructions. We also now store all of our inputs in hex, so it doesn't matter if we read an exe or text file - the method is the same

Good: Achieved Sprint Goals

Could Have Done Better: n/a

Area of Improvement: Indexing method could be better

# User guide/Demo

1. Step 1: Input FilePath and what word you are searching for as arguments. If the indexed file was not found in the folder you are searching in.
   It will prompt you to press x to continue or i to index the files. indexing will take longer in the begging but once you do that once you dont have to do it again.

![UserGuide-Step1](imgs/Userguide-1.PNG "Step-1")

2. Step 2: If you choose to index the file it will show the progress bar of the indexing.

![UserGuide-Step2](imgs/Userguide-2.PNG "Step-2")

3. Step 3: Once you run it again it will prompt you to chose the word/phrase to search for. After you enter the word/phrase it will send you the results and where they are located at.

![UserGuide-Step3](imgs/Userguide-3.PNG "Step-3")
![UserGuide-Step3](imgs/Userguide-4.PNG "Step-3")

# Project Management

We will follow he Scrum approach, thus your team needs to identify the task in each sprint cycles, team meeting schedules, including this Fall and next Spring semester. The planned schedule and sprint cycles for Spring 2021 are as follows.
As in Shown in our diagrams, we have divided our work into 4 Sprints. Our basic approach is as follows:

Sprint 0-1: Become re-familiarized with our language of choice for project and research further into potential efficient searching algorithms, such as N-gram

Sprint 2: In this Sprint, we plan to have a basic "Alpha version" of our project available in order to give us a general idea of how our current approach is working. Our overall goal of this sprint is to have all required methods of input mastered. This includes Hexadecimal instructions and basis strings.

Sprint 3: In this Sprint, we will have taken our "Alpha version" through a process of revision and have a newer and better version to test.

Sprint 4: In this Sprint we will first put the finishing project on our design and make sure it meets function requirements. Then, any time we have left, we will attempt to implement our high level use cases.

![Spring 2021 Timeline](https://capstones-cs-udayton.bitbucket.io/imgs/cps491s21timeline.png "Spring 2021 Timeline")

Below is a current example of how our Trello Board overview looks with our proposed plans for each sprint (Under "Product Backlog")

![Trello Overview](imgs/trello-overview.PNG "Trello Overview")

Below is the example of the Trello board timeline (Gantt chart) with sprint cycles with a few of our intended tasks for Spring 2021:

![Spring 2021 Timeline on Trello](imgs/Gantt_Chart.PNG "Spring 2021 Timeline")

# Company Support

GE Aviation will aid us in provding the necessary malware needed to do the project, as well as the basic understanding and layout of how the tool will need ot be. Our mentor, Jeff Archer, from GE Aviation has
offered to be available to answer any questions. We plan to regularly keep in touch via Microsoft Teams with our mentor in order to ensure that adequate progress is being achieved and that we are correctly meeting
the specified requirements that our company has put in place.
