# Test.py

    import ngram
    import threading
    import time
    import os
    import sys
    import getopt
    from progress.bar import ChargingBar
    thread_lock = threading.Lock()
    shared_Files = []
    searchElements = []
    threads = []
    search_Threads = []
    key = " "  # the user search input
    foundDestination = set()
    foundList = []
    count = 0
    already_visited = set()
    destinations = {}
    # thread to parse incoming files
    indexcounter = 0


    class parser (threading.Thread):

        # initialize thread
        def __init__(self, threadID, name, input_length, mode):
            threading.Thread.__init__(self)
            self.threadID = threadID
            self.name = name
            self.files = []
            self.lines = []
            self.parse_length = input_length
            self.mode = mode  # determines whether we index or search on the fly

        # running protocol of thread
        def run(self):
            # take file from shared queue of files
            print("running", self.name)
            # thread_lock.acquire()
            while True:

                if len(shared_Files) == 0:
                    break
                else:
                    if self.mode == 0:
                        self.parseLines(shared_Files.pop())

                    else:
                        self.indexFile(shared_Files.pop())
                        # index the file

        # used for indexing approach - index file into n elements
        # combines n files into 1 file with indexed elements

        def indexFile(self, theFile):
            # see if indexed file exists - for safety
            temp = []
            file_name = "indexed.txt"
            current_path = sys.argv[1]
            complete_path = os.path.join(current_path, file_name)

            if os.path.exists(complete_path):
                try:
                    File = open(theFile, "r")
                except FileNotFoundError:
                    pass
                else:
                    # acquire lock (safety) and append individual elements
                    for words in File:
                        line = words.split()
                        for i in line:
                            temp.append(i)
                try:
                    write_file = open(complete_path, "a")
                except FileNotFoundError:
                    pass
                else:
                    thread_lock.acquire()
                    for words in temp:
                        global indexcounter
                        indexcounter = indexcounter + 1
                        #write_file.write(words + "-" + theFile + " ")
                        write_file.write(words + "$" + theFile + " ")
                    write_file.write("\n")
                    write_file.close()
                    IndexFile.next()
                    thread_lock.release()

        # parse file into ngrams, return list

        def parseLines(self, theFile):
            overflow_lines = []
            search_string = " "
            # print(theFile, "in thread ", self.name)
            try:
                File = open(theFile, "r")
            except FileNotFoundError:
                pass
            else:
                global searchElements
                global already_visited
                global destinations
                # print(theFile)
                # thread_lock.acquire()
                for words in File:
                    line = words.split()

                    # if one element search, only append elements of line with matching size to possibly speed search

                    if key.find(" ") == -1:
                        for i in range(len(line)):
                            size = len(line[i])

                            # if beginning character of key is not in begining spot of target string, forget it
                            # i.e first character of pizza 'p' compared to string izzap p =/= i (pizza will never be izzap)

                            first_character = key[0]
                            same = line[i][0] is first_character

                            # then check to see if element is already in search elements, if it is, do not add (avoid duplicates)

                            # attempt adding newest element to set - if size remains same, elements has already been added to search_elements
                            current_size = len(already_visited)
                            already_visited.add(line[i])

                            if current_size == len(already_visited):
                                # already added to search elements
                                visited = 1

                            if current_size != len(already_visited):
                                # not added to search elements
                                visited = 0

                            if len(key) == size:
                                element = line[i]
                                the_term = destinations.get(element)

                                if same:
                                    # x
                                    # print(element)
                                    if visited == 0:
                                        # create new element and add it to destination set
                                        the_element = matchElement(element)
                                        the_element.set_locations(theFile)
                                        searchElements.append([element])
                                        destinations[element] = the_element
                                        already_visited.add(element)

                                    if visited == 1:
                                        # print(theFile)
                                        if the_term is not None:
                                            # print(theFile)
                                            thread_lock.acquire()
                                            the_term.set_locations(theFile)
                                            thread_lock.release()

                    # if size of key is greater than one, we want a multi word search
                    if key.find(" ") != -1:
                        # length of the current line from file
                        line_size = len(line)
                        # number of elements (individual words) in search string
                        key_size = len(key.split())
                        # the number of leftover words from built search string
                        overflow = key_size - line_size

                        # first check to see if there's any overflow leftover from previous line
                        # for i in overflow_lines:
                        #     search_string += i + " "

                        # if not or after that then begin concatenating elements of current line
                        for x in range(line_size):
                            overflow = line_size - x

                            # if string has already reached limit (i.e due to past overflow) break and add all remaining elements in line to    overflow

                            if len(search_string.split()) > key_size:

                                for overflow in range(line_size):
                                    overflow_lines.append(line[overflow])
                                break

                            # if number of words less than number of words in search input string, concatenate

                            if x < key_size:
                                search_string += line[x] + " "
                            else:
                                overflow_lines.append(line[x])

                        searchElements.append(search_string)
                        search_string = " "
                # thread_lock.release()
                    # self.lines.append(line)
                    # searchElements.append(line)


    # thread to search recently parsed lines
    class matchThread(threading.Thread):
        def __init__(self, name):
            threading.Thread.__init__(self)
            self.name = name

        def run(self):
            time.sleep(0.5)
            print("search", self.name)
            while True:
                if len(searchElements) == 0:
                    break
                else:
                    line = searchElements.pop()
                    search = ngram.NGram(line)
                    found = search.search(key, 0.3)
                    if found:
                        foundList.append(found)
                        element = destinations.get(line[0])
                        locations = element.locations
                        thread_lock.acquire()
                        for i in locations:
                            foundDestination.add(i)
                        thread_lock.release()
    # if we have indexed file, load elements into map


    class calibrate_map(threading.Thread):
        def __init__(self, name):
            threading.Thread.__init__(self)
            self.name = name

        def run(self):
            time.sleep(1)

            # load elements from indexed file into map

    # a class of search element that keeps track of all locations


    class matchElement:
        def __init__(self, term):
            self.search_term = term
            self.locations = set()
            # a list of all locations (files) where an element appears

        def set_locations(self, location):
            self.locations.add(location)


    if __name__ == "__main__":
        file_name = "indexed.txt"
        # current_path = os.path.dirname(os.path.realpath(__file__))
        # complete_path = os.path.join(current_path, file_name + ".txt")
        current_path = sys.argv[1]
        complete_path = []
        for entry in os.listdir(current_path):
            if os.path.isfile(os.path.join(current_path, entry)):
                complete_path.append(entry)
        # check to see if we have an indexed file
        indexed = os.path.exists(os.path.join(current_path, file_name))

        # the user search term
        # key = input("Enter Word/Phrase to search for:  ")

        if indexed:
            # use indexed file to do search
            # calibrate map with the found indexed file
            search_map = {}
            print("Calibrating..")  # ideally done in advance
            file = open(os.path.join(current_path, file_name), "r")
            # Calibrating = ChargingBar('Calibrating', max=indexcounter)
            i = 0
            for words in file:
                line = words.split()
                for a_word in line:
                    word_length = len(a_word)

                    # if a_word not length of key, do not add
                    # if word_length != 4:
                    #     continue

                    # the separator between term and location
                    #delim = a_word.find("-")
                    delim = a_word.find("$")
                    # according to index file: search term from 0 - delim ("-"), location from delim to end

                    search_term = a_word[0:delim]
                    # print(search_term)
                    location = a_word[delim:word_length]

                    element = matchElement(search_term)

                    the_element = search_map.get(search_term)
                    # check to see if search term is in map first
                    if search_term in search_map:

                        # only add location if not there (add to set)

                        the_element.set_locations(location)

                    if search_term not in search_map:
                        # if not then append new term to map
                        element.set_locations(location)
                        search_map[search_term] = element
                # Calibrating.next()

            # Calibrating.finish()
            # then simply do a search on this map with key - O(1)
            key = input("Enter Word/Phrase to search for:  ")
            # key = sys.argv[2]
            start_time = time.time()
            search_result = search_map.get(key)
            print("%s results returned in %s ms" % (
                len(search_result.locations), round(time.time() - start_time) * 1000))
            for item in search_result.locations:
                print(item)

        else:
            # define needed variables
            i = 1
            input_size = len(key)

            response = input(
                "No indexed file was found.. press 'x' to continue  search or 'i' to index files: ")
            start_time = time.time()

            if response == "i":
                # if user requests index, first create an index file, then index the files
                file_name = "indexed.txt"
                file = open(os.path.join(current_path, file_name), "w")
                file.write("0")
                file.close()
               # for i in range(1000):
               #     shared_Files.append(
               #         "Test\LoremFiles\lorem1000\lorem%i.txt" % i)
                for File in complete_path:
                    shared_Files.append("%s\%s" % (current_path, File))
                IndexFile = ChargingBar('Indexing', max=len(shared_Files))
                t1 = parser(1, "Thread1", 0, 1)
                t2 = parser(2, "Thread2", 0, 1)
                t3 = parser(3, "Thread3", 0, 1)
                t4 = parser(4, "Thread4", 0, 1)
                threads.append(t1)
                threads.append(t2)
                threads.append(t3)
                threads.append(t4)

                for thread in threads:
                    thread.start()
                for the_thread in threads:
                    the_thread.join()

                IndexFile.finish()

            else:
                # key = input("Enter Word/Phrase to search for:  ")
                key = sys.argv[2]

                # we search the files with no prior indexing
             #   for i in range(1000):
             #       shared_Files.append(
             #           "Test\LoremFiles\lorem1000\lorem%i.txt" % i)
                for File in complete_path:
                    shared_Files.append("%s\%s" % (current_path, File))
                start_time = time.time()
                t1 = parser(1, "Thread1", input_size, 0)
                t2 = parser(2, "Thread2", input_size, 0)
                t3 = parser(3, "Thread3", input_size, 0)
                t4 = parser(4, "Thread4",  input_size, 0)
                search1 = matchThread("search1")
                search2 = matchThread("search2")
                search3 = matchThread("search3")
                search4 = matchThread("search4")

                t1.start()
                t2.start()
                t3.start()
                t4.start()
                search_Threads.append(search1)
                search_Threads.append(search2)
                search_Threads.append(search3)
                search_Threads.append(search4)
                threads.append(t1)
                threads.append(t2)
                threads.append(t3)
                threads.append(t4)
                for thread in threads:
                    thread.join()
                search1.start()
                search2.start()
                search3.start()
                search4.start()
                for thread in search_Threads:
                    thread.join()

        # print("%d results returned in %s ms" % (round(time.time() - start_time) * 1000))
        # sync threads
        # print(foundDestination)
        # print(foundList)
        if len(sys.argv) > 3 and sys.argv[3] == '-o':
            print("Results are in: " + sys.argv[4])
            sys.stdout = open(sys.argv[4], 'w')

        if len(foundList) > 0:
            print("%s results found in %s ms" %
                  (len(foundDestination), round(time.time() - start_time) * 1000))
        for i in foundList:
            print(i)
        for i in foundDestination:
            print(i)

        # end non-indexing implementation
